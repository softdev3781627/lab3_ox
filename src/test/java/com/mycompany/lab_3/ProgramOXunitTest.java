/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab_3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class ProgramOXunitTest {

    public ProgramOXunitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void TestCheckWinNoPlayBy_O_output_false() {
        String[][] table = {{"-", "-", "-",}, {"-", "-", "-",}, {"-", "-", "-",}};
        String currentPlayer = "O";
        assertEquals(false, Lab_3.checkWin(table, currentPlayer));
    }

    @Test
    public void TestCheckWinRowBy_O_output_true() {
        String[][] table = {{"-", "-", "-",}, {"O", "O", "O",}, {"-", "-", "-",}};
        String currentPlayer = "O";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }
    @Test
    public void TestCheckWinRow2By_O_output_true() {
        String[][] table = {{"O", "O", "O",}, {"-", "-", "-",}, {"-", "-", "-",}};
        String currentPlayer = "O";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }
    @Test
    public void TestCheckWinRow3By_O_output_true() {
        String[][] table = {{"-", "-", "-",}, {"-", "-", "-",}, {"O", "O", "O",}};
        String currentPlayer = "O";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }

    @Test
    public void TestCheckWinRowBy_X_output_true() {
        String[][] table = {{"-", "-", "-",}, {"X", "X", "X",}, {"-", "-", "-",}};
        String currentPlayer = "X";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }

    @Test
    public void TestCheckWinColBy_O_output_true() {
        String[][] table = {{"O", "X", "-",}, {"O", "X", "-",}, {"O", "-", "-",}};
        String currentPlayer = "O";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }

    @Test
    public void TestCheckWinColBy_X_output_true() {
        String[][] table = {{"X", "O", "-",}, {"X", "O", "-",}, {"X", "-", "-",}};
        String currentPlayer = "X";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }

    @Test
    public void TestCheckWinDiagonal1By_X_output_true() {
        String[][] table = {{"X", "O", "-",}, {"O", "X", "-",}, {"-", "-", "X",}};
        String currentPlayer = "X";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }

    @Test
    public void TestCheckWinDiagonal1By_O_output_true() {
        String[][] table = {{"O", "X", "-",}, {"X", "O", "-",}, {"-", "-", "O",}};
        String currentPlayer = "O";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }

    @Test
    public void TestCheckWinDiagonal2By_X_output_true() {
        String[][] table = {{"-", "-", "X",}, {"O", "X", "-",}, {"X", "-", "-",}};
        String currentPlayer = "X";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }

    @Test
    public void TestCheckWinDiagonal2By_O_output_true() {
        String[][] table = {{"-", "-", "O",}, {"X", "O", "-",}, {"O", "-", "-",}};
        String currentPlayer = "O";
        assertEquals(true, Lab_3.checkWin(table, currentPlayer));
    }
    @Test
    public void TestCheckDraw1By_O_output_true() {
        String[][] table = {{"O", "X", "O",}, {"X", "O", "X",}, {"X", "O", "X",}};
        String currentPlayer = "O";
        assertEquals(true, Lab_3.isDraw(table, currentPlayer));
    }
    @Test
    public void TestCheckDraw2By_X_output_true() {
        String[][] table = {{"X", "O", "X",}, {"O", "X", "O",}, {"O", "X", "O",}};
        String currentPlayer = "X";
        assertEquals(true, Lab_3.isDraw(table, currentPlayer));
    }

}
